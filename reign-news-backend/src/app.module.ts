import { Module } from '@nestjs/common';
import { PostsModule } from './posts/posts.module';
import { MongooseModule } from '@nestjs/mongoose';
import { ScheduleModule } from '@nestjs/schedule';

@Module({
  imports: [
    PostsModule,
    MongooseModule.forRoot(`mongodb://mongo:27017/mongo-test`),
    ScheduleModule.forRoot(),
  ],
})
export class AppModule {}
