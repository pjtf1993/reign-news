import { HttpService, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Post, PostDocument } from './schemas/posts.schema';
import { map } from 'rxjs/operators';
import { Cron } from '@nestjs/schedule';

@Injectable()
export class PostsService {
  constructor(
    @InjectModel(Post.name) private postModel: Model<PostDocument>,
    private httpService: HttpService,
  ) {}

  @Cron('0 * * * *')
  async fetchPosts(): Promise<Post[]> {
    const fetchedPosts = await this.httpService
      .get('http://hn.algolia.com/api/v1/search_by_date?query=nodejs')
      .pipe(
        map((response) => {
          return response.data.hits;
        }),
      )
      .toPromise();

    return Promise.all(
      fetchedPosts.map(async (post) => {
        const oldPost = await this.postModel.exists({
          created_at: post.created_at,
        });
        console.log(oldPost);

        if (!oldPost) {
          const posts = new this.postModel(post).save();
          console.log('posts saved');
          return posts;
        } else {
          console.log('post was not saved');

          return null;
        }
      }),
    );
  }

  async findAll(): Promise<Post[]> {
    const posts = await this.postModel
      .find({ isDeleted: false })
      .sort('-created_at')
      .exec();
    console.log('pasando por findall');
    return posts;
  }

  async remove(id: string): Promise<any> {
    const deletedPost = await this.postModel.findByIdAndUpdate(id, {
      isDeleted: true,
    });
    return deletedPost;
  }
}
