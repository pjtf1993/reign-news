import {
  Controller,
  Get,
  Param,
  Delete,
  NotFoundException,
} from '@nestjs/common';
import { PostsService } from './posts.service';
import { Posts } from './entities/post.entity';

@Controller('posts')
export class PostsController {
  constructor(private readonly postsService: PostsService) {}

  @Get('/fetch')
  async getPosts(): Promise<Posts[]> {
    const posts = await this.postsService.fetchPosts();
    console.log('post saved');
    return posts;
  }

  @Get()
  async findAll(): Promise<Posts[]> {
    return this.postsService.findAll();
  }

  @Delete('/delete/:id')
  async deletePost(@Param('id') id: string) {
    const deletedPost = await this.postsService.remove(id);
    if (!deletedPost) {
      throw new NotFoundException('Post does not exist!');
    }
    return deletedPost;
  }
}
