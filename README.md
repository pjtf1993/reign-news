## Reig news

Reign news is a web app that fetchs every hour the recently posted articles about Node.js on Hacker News:

## The stack

Node.Js v14.15.5 LTS
Nest.Js v7.5.1
Mongoose v5.11.15
MongoDB
React.Js v17.0.1
Sass
Axios
Date-fns
Docker


In order to make the app to work you just need to do the following:

```
 git clone https://gitlab.com/pjtf1993/reign-news.git

docker-compose up

go to http://localhost:3000/

to close the app just type docker-compose down

 ```

You must have installed Docker in your computer in order for this to work. Other than that you don't need to setup anything else, just open http://localhost:3000/ and click the button to fetch new posts.


