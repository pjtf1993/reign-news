import React from 'react';

import './App.scss';
import NavBar from './components/Navbar';
import PostList from './components/PostList';

function App() {
  return (
    <>
      <NavBar />
      <PostList />
    </>
  );
}

export default App;
