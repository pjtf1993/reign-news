import React from 'react';
import { formatDistanceToNow, parseISO } from 'date-fns';
import { FaTrash } from 'react-icons/fa';

const Post = ({ data, deletePost }) => {
  return (
    <>
      {data.title || data.story_title ? (
        <div className="post-container">
          <div className="post-box">
            <a
              href={data?.story_url ? data?.story_url : data?.url}
              target="_blank"
              rel="noopener noreferrer"
            >
              <h3>{data?.story_title ? data?.story_title : data?.title}</h3>
              <span>- {data?.author} -</span>
            </a>
          </div>
          <div className="post-time">
            <span>{formatDistanceToNow(parseISO(data?.created_at))}</span>
            <button
              className="delete-button"
              onClick={() => deletePost(data?._id)}
            >
              <FaTrash />
            </button>
          </div>
        </div>
      ) : null}
    </>
  );
};

export default Post;
