import React, { useEffect, useState } from 'react';
import Post from './Post';
import axios from 'axios';

const PostList = () => {
  const [data, setData] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [isError, setIsError] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      setIsError(false);
      setIsLoading(true);
      try {
        const result = await axios('http://localhost:8080/posts/');
        setData(result.data);
      } catch (error) {
        console.log(error);
        setIsError(true);
      }
      setIsLoading(false);
    };
    fetchData();
  }, []);

  const fetchInitialPosts = async () => {
    setIsLoading(true);
    try {
      const result = await axios('http://localhost:8080/posts/fetch');
      setData(result.data);
    } catch (error) {
      console.log(error);
    }
    setIsLoading(false);
  };

  const deletePost = async (id) => {
    console.log(id);
    try {
      const res = await axios.delete(
        `http://localhost:8080/posts/delete/${id}`
      );

      if (res.status === 200) {
        const newData = data.filter((x) => x._id !== id);
        setData(newData);
      }
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <>
      {data.length > 0 && !isLoading && !data.includes(null) ? (
        data?.map((x) => {
          return <Post key={x?._id} data={x} deletePost={deletePost} />;
        })
      ) : isLoading ? (
        <div className="load-info">
          <h1>Loading...</h1>
        </div>
      ) : data.length === 0 || isError ? (
        <div className="load-info">
          <h1>You can click here to load the initial posts</h1>
          <button onClick={fetchInitialPosts}>Load posts</button>
        </div>
      ) : (
        <div className="load-info">
          <h1>It seems that there are no more posts</h1>
        </div>
      )}
    </>
  );
};

export default PostList;
