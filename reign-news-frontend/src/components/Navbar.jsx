import React from 'react';

const NavBar = () => {
  return (
    <>
      <header className="navbar">
        <h1>HN Feed</h1>
        <h2>We {'<3'} hacker news!</h2>
      </header>
    </>
  );
};

export default NavBar;
